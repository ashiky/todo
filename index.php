<?php include('conn.php');
  if(isset($_POST["action"]) &&  $_POST["action"] == "add" ){
        $description = $_POST['description'];
        // Création de la requête
        $requete = "INSERT INTO `Taches` (`description`) VALUES (:description);";
        $query = $conn->prepare($requete);
        $query->bindParam(':description', $description);
        $result = $query->execute();
  }
  if(isset($_GET["action"]) &&  $_GET["action"] == "chec" ){
    var_dump($_GET);
    $statut = isset($_GET['statut']) ? 1 : 0;
    var_dump($statut);
        $requete = "UPDATE Taches SET statut = :statut WHERE id = :id";
        $query = $conn->prepare($requete);
        $query->bindParam(':statut',$statut);
        $query->bindParam(':id', $_GET["id"]);
        $result = $query->execute();
        var_dump($result);
        if ($result) {
            echo "Le statut à bien été changé";
        }
        else {
            echo 'Erreur';
        }
    }
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>Todo</title>
</head>

<body>
    <main>
        <h1>Todo</h1>
        <div class="container">
            <h2>Ajouter une tache :</h2>
            <form action="" method="post">
                <input type="text" name="description">
                <input class='textInput'type="hidden" name="action" value="add" />
                <input type="submit">
            </form>
        </div>
        <div class="container">
            <h2>Tâches :</h2>
            <?php
            $requete = "SELECT * from Taches";
            $data = $conn->query($requete);
            foreach ($data as $row) {
               $checked = $row['statut'] == 1 ?"checked" :"" ;
               $bar = $row['statut'] == 1 ?"bar" :"" ;
            ?>
                    <div class="taches">
                        <form action="" method="GET">
                            <input type="checkbox" name="statut" <?php echo $checked?>>
                            <input type="hidden" name="id" value="<?php echo $row['id']?>"/>
                            <input type="submit" name="action" value="chec">
                        </form>
                        <p  class="<?php echo $bar ?>"><?php echo $row['description'];
                        ?></p>
                    </div>
            <?php } ?>

        </div>
    </main>
</body>

</html>